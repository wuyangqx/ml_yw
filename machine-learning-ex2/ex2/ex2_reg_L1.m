%% Machine Learning Online Class - Exercise 2: Logistic Regression
%
%  Instructions
%  ------------
%
%  This file contains code that helps you get started on the second part
%  of the exercise which covers regularization with logistic regression.
%
%  You will need to complete the following functions in this exericse:
%
%     sigmoid.m
%     costFunction.m
%     predict.m
%     costFunctionReg.m
%
%  For this exercise, you will not need to change any code in this file,
%  or any other files other than those mentioned above.
%

%% Initialization
clear ; close all; clc

%% Load Data
%  The first two columns contains the X values and the third column
%  contains the label (y).

data = load('ex2data2.txt');
X = data(:, [1, 2]); y = data(:, 3);

plotData(X, y);

% Put some labels
hold on;

% Labels and Legend
xlabel('Microchip Test 1')
ylabel('Microchip Test 2')

% Specified in plot order
legend('y = 1', 'y = 0')
hold off;


%% =========== Part 1: Regularized Logistic Regression ============
%  In this part, you are given a dataset with data points that are not
%  linearly separable. However, you would still like to use logistic
%  regression to classify the data points.
%
%  To do so, you introduce more features to use -- in particular, you add
%  polynomial features to our data matrix (similar to polynomial
%  regression).
%

% Add Polynomial Features

% Note that mapFeature also adds a column of ones for us, so the intercept
% term is handled
X = mapFeature(X(:,1), X(:,2));

%% ============= Part 2: Regularization and Accuracies =============
%  Optional Exercise:
%  In this part, you will get to try different values of lambda and
%  see how regularization affects the decision coundart
%
%  Try the following values of lambda (0, 1, 10, 100).
%
%  How does the decision boundary change when you vary lambda? How does
%  the training set accuracy vary?
%

% Initialize fitting parameters
initial_theta = zeros(size(X, 2), 1);

% Set regularization parameter lambda to 1 (you should vary this)
%lambda = 1;

for i=1:12
    lambda=50/(2^i);
    Lam(i)=lambda;

% Set Options
options = optimset('GradObj', 'on', 'MaxIter', 400);

% Optimize
[theta, J, exit_flag] = ...
	fminunc(@(t)(costFunctionReg(t, X, y, lambda)), initial_theta, options);
[theta_L1, J_L1, exit_flag_L1] = ...
	fminunc(@(t)(costFunctionReg_L1(t, X, y, lambda)), initial_theta, options);

%% Plot Boundary
% plotDecisionBoundary(theta, X, y);
% 
% hold on;
% title(sprintf('lambda = %g, L2-reg', lambda))
% 
% % Labels and Legend
% xlabel('Microchip Test 1')
% ylabel('Microchip Test 2')
% 
% legend('y = 1', 'y = 0', 'Decision boundary')
% hold off;
% 
% 
% plotDecisionBoundary(theta_L1, X, y);
% hold on;
% title(sprintf('lambda = %g, L1-reg', lambda))
% 
% % Labels and Legend
% xlabel('Microchip Test 1')
% ylabel('Microchip Test 2')
% 
% legend('y = 1', 'y = 0', 'Decision boundary')
% hold off;

%% Compute accuracy on our training set
p = predict(theta, X);
% fprintf('Train Accuracy(L2): %f\n', mean(double(p == y)) * 100);
% fprintf('Sparsity (L2): %f\n', mean(double(abs(theta) < 1e-6)) * 100);
p_L1 = predict(theta_L1, X);
% fprintf('Train Accuracy(L1): %f\n', mean(double(p_L1 == y)) * 100);
% fprintf('Sparsity (L1): %f\n', mean(double( abs(theta_L1) < 1e-6)) * 100);

acu(i)= mean(double(p == y));
acu_L1(i)= mean(double(p_L1 == y));
spar(i)= mean(double( abs(theta) < 1e-6));
spar_L1(i)= mean(double( abs(theta_L1) < 1e-6));
end

%%
figure
semilogx(Lam,acu,'c--*', Lam, acu_L1, 'b--o');
legend('L2','L1');
xlabel('\lambda');
ylabel('accuracy on training data');
title('accuracy vs \lambda');

figure
semilogx(Lam,spar,'c--*', Lam, spar_L1, 'b--o');
legend('L2','L1');
xlabel('\lambda');
ylabel('percentage of zeros in \theta');
title('sparsity in \theta');


