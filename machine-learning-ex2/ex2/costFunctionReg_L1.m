function [J, grad] = costFunctionReg_L1(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

[J,grad]=costFunction(theta,X,y);

% update J from regularization
n1=size(theta,1); % n+1
s=norm(theta(2:n1),1);
J=J+lambda/(2*m)*s;

% update gradient from regularization 
grad(2:n1)=grad(2:n1)+lambda/m*sign(theta(2:n1));


% =============================================================

end
