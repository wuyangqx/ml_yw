function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

% unregularized part
tmp=X*theta-y;
J=tmp'*tmp/(2*m);

grad=X'*tmp/m; 

% update J from regularization
sum=theta'*theta-theta(1)*theta(1);
J=J+lambda/(2*m)*sum;

% update gradient from regularization 
n1=size(theta,1); % n+1
grad(2:n1)=grad(2:n1)+lambda/m*theta(2:n1);

% =========================================================================

grad = grad(:);

end
